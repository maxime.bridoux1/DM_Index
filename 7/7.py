import os
import cv2
import numpy as np
from matplotlib import pyplot as plt
from scipy.spatial import distance
from time import time
from statistics import mean


print("----------------------------------------------------------------------")
folder = '/home/maxime/Bureau/OneDrive/DM/DM_Index/Données Index/Images/Copydays/'
img1 = cv2.imread(folder + 'Copydays_original_210700_512.jpg',cv2.IMREAD_GRAYSCALE)
img2 = cv2.imread(folder + 'Copydays_strong_214301_512.jpg',cv2.IMREAD_GRAYSCALE)


hist1 = cv2.calcHist([img1],[0],None,[256],[0,256])

def read_database(dim):
    x = []
    y = []
    for image_name in os.listdir(folder):
        image = cv2.imread(folder + image_name,cv2.IMREAD_GRAYSCALE)
        hist = cv2.calcHist([image],[0],None,[dim],[0,256])
        if image_name.find("_original_") != -1:
            y.append(transform_hist_vect(hist))
        else:
            x.append(transform_hist_vect(hist))
    return [x,y]
    
def transform_hist_vect(hist):
    vect = []
    for i in range(len(hist)):
        vect.append(hist[i][0])
    return vect            

#M=157
#N=3212

def compute_ratios(i,x,y):
    z=distance.cdist(x,y,'sqeuclidean') # la distance euclidienne. 
        # 'sqeuclidean' donnerait la dist euclidienne au carre 
        
    minimums=z[:,0:i].min(1)
    maximums=z[:,0:i].max(1)
    ratios=minimums/maximums
    return ratios

def plot_method_a():
    X = []
    Y = []
    [x,y] = read_database(256)
    
    for i in [5,10,50,100,150,200,256]:
        top = time()
        ratios = compute_ratios(i,x,y)
        
        print("temps de calcul =", time()-top)
        X.append(i)
        Y.append(ratios)
        
    plt.plot(X,Y)
    plt.xlabel("Dimension D")
    plt.ylabel("Ratios")
    plt.show()
        
def plot_method_b():
    X = []
    Y = []
    for i in [5,10,15,100,150,200,256]:
        top = time()
        
        [x,y] = read_database(i)
        ratios = compute_ratios(256,x,y)

        print("temps de calcul =", time()-top)

        X.append(i)
        Y.append(mean(ratios))
        
    plt.plot(X,Y)
    plt.xlabel("Dimension D")
    plt.ylabel("Ratios")
    plt.show()