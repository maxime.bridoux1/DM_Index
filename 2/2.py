import numpy as np
import scipy.spatial.distance as dist
from math import sqrt

def generate_vectors(N,D):
    x = np.random.randn(N,D)
    zero = [np.zeros(D) for i in range(N)]
    norms = dist.cdist(x, zero, 'sqeuclidean')
    for i in range(N):
        a = sqrt(norms[i][0])
        for j in range(D):
            x[i][j] = abs(x[i][j]/a)
    return x;

#vérification
#dist.cdist(generate_vectors(5,5), [np.zeros(5)], 'sqeuclidean')
