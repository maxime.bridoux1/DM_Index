import os
import cv2
import numpy as np
from matplotlib import pyplot as plt
from scipy.spatial import distance
from time import time
from statistics import mean
from math import sqrt

def generate_vectors(N,D):
    x = np.random.randn(N,D)
    zero = [np.zeros(D) for i in range(N)]
    norms = distance.cdist(x, zero, 'sqeuclidean')
    for i in range(N):
        a = sqrt(norms[i][0])
        for j in range(D):
            x[i][j] = abs(x[i][j]/a)
    return x;

print("----------------------------------------------------------------------")
folder = '/home/maxime/Bureau/OneDrive/DM/DM_Index/Données Index/Images/Copydays/'
img1 = cv2.imread(folder + 'Copydays_original_210700_512.jpg',cv2.IMREAD_GRAYSCALE)
img2 = cv2.imread(folder + 'Copydays_strong_214301_512.jpg',cv2.IMREAD_GRAYSCALE)

def read_database(dim):
    x = []
    y = []
    for image_name in os.listdir(folder):
        image = cv2.imread(folder + image_name,cv2.IMREAD_GRAYSCALE)
        hist = cv2.calcHist(image, [0, 1, 2], None, [dim, dim, dim], [0, 256, 0, 256, 0, 256])
        cv2.normalize(hist,hist, norm_type=cv2.NORM_L2)
        if image_name.find("_original_") != -1:
            y.append(transform_hist_vect(hist, dim))
        else:
            x.append(transform_hist_vect(hist, dim))
    return [x,y]
    
def transform_hist_vect(hist, dim):
    vect = []
    for i in range(dim):
        for j in range(dim):
            for k in range(dim):
                vect.append(hist[i][j][k])
    return vect            

def compute_ratios(x,y):
    z=distance.cdist(x,y,'sqeuclidean') # la distance euclidienne. 
        # 'sqeuclidean' donnerait la dist euclidienne au carre 
    minimums=z.min(1)
    maximums=z.max(1)
    ratios=minimums/maximums
    return ratios
        
def plot_method_b_both_data():
    X = []
    Y = []
    dimensions = [2,4,8,10,13,16,20] 
    for i in dimensions: #[8, 64, 200, 256, 512, 1000, 2197, 4096, 8000]
        top = time()
        
        [x,y] = read_database(i)
        ratios = compute_ratios(x,y)

        print("Dimension :", i**3)
        print("Temps de calcul :", time()-top)

        X.append(i**3)
        Y.append(mean(ratios))
        
    p1=plt.plot(X,Y,"g-")
    
    X = []
    Y = []
    
    for i in dimensions:
        x=generate_vectors(len(x),i**3) # les donnees
        y=generate_vectors(len(y),i**3) # les requetes
        ratios=compute_ratios(x,y)
    
        print("Dimension :", i**3)
        print("temps de calcul =", time()-top)
        X.append(i**3)
        Y.append(mean(ratios))
    
    p2=plt.plot(X,Y,"r-")
    plt.xlabel("Dimension D")
    plt.ylabel("Ratios moyens")
    plt.legend([p1[0],p2[0]],["Données réelles", "Données synthétiques"], loc = "best")
    plt.show()
#N = 3051
#M = 157