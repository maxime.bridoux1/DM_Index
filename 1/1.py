import numpy as np
import scipy.spatial.distance as dist
import matplotlib.pyplot as plt
from time import time


#M=100
#N=10000
#
#X = []
#Y = []
#
#for i in range(4,13):
#    top = time()
#    D = 2**i
#    print("D =", D)
#    x=np.random.randn(N,D) # les donnees
#    y=np.random.randn(M,D) # les requetes
#    z=dist.cdist(x,y,'euclidean') # la distance euclidienne. 
#    # 'sqeuclidean' donnerait la dist euclidienne au carre 
#    
#    minimums=z.min(1)
#    maximums=z.max(1)
#    ratios=minimums/maximums
#    
#    print("temps de calcul =", time()-top)
#    X.append(D)
#    Y.append(ratios)
#    
#plt.plot(X,Y)
#plt.xlabel("Dimension D")
#plt.ylabel("Ratios")
#plt.show()


X = []
Y = []
D = 512
N = 1000
top = time()
for M in range(1,100):
    x=np.random.randn(N,D) # les donnees
    y=np.random.randn(M,D) # les requetes
    z=dist.cdist(x,y,'euclidean') # la distance euclidienne. 
    # 'sqeuclidean' donnerait la dist euclidienne au carre 
    
    minimums=z.min(1)
    maximums=z.max(1)
    ratios=minimums/maximums
    
    
    X.append(M)
    Y.append(ratios)
print("temps de calcul =", time()-top)
plt.plot(X,Y)
plt.xlabel("Nombre de requêtes M")
plt.ylabel("Ratios")
plt.show()