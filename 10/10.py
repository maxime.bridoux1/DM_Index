import os
import cv2
import numpy as np
from matplotlib import pyplot as plt
from scipy.spatial import distance
from time import time
from statistics import mean
    
SQRT_2 = np.sqrt(2)    
    
def hellinger_squared(p, q):
    return np.sum((np.sqrt(p) - np.sqrt(q)) ** 2) / 2
    
def normalise(a):
    norm = np.sqrt(sum(a)/SQRT_2)
    b = []
    for x in a:
        b.append(x/norm)
    return b
    
def hell_distance(x,y):
    z = np.array([])
    for p in x:
        a = np.array([])
        for q in y:
            a = np.append(a, hellinger_squared(p,q))
        z = np.append(z, a)
    return np.reshape(z, ((len(x),len(y))))


folder = '/home/maxime/Bureau/OneDrive/DM/DM_Index/Données Index/Images/Copydays/'

def read_database(dim):
    x = []
    y = []
    for image_name in os.listdir(folder):
        image = cv2.imread(folder + image_name,cv2.IMREAD_GRAYSCALE)
        hist = cv2.calcHist([image],[0],None,[dim],[0,256])
        if image_name.find("_original_") != -1:
            y.append(normalise(transform_hist_vect(hist)))
        else:
            x.append(normalise(transform_hist_vect(hist)))
    return [x,y]
    
def transform_hist_vect(hist):
    vect = []
    for i in range(len(hist)):
        vect.append(hist[i][0])
    return vect            

#M=157
#N=3212

def compute_ratios(i,x,y):
    z=hell_distance(x,y)
    print(i)
        # 'sqeuclidean' donnerait la dist euclidienne au carre 
        
    minimums=z[:,0:i].min(1)
    maximums=z[:,0:i].max(1)
    ratios=minimums/maximums
    return ratios

def plot_method_a():
    X = []
    Y = []
    [x,y] = read_database(256)
    print("database read")
    
    for i in [5,10,50,100,150,200,256]:
        top = time()
        ratios = compute_ratios(i,x,y)
        
        print("temps de calcul =", time()-top)
        X.append(i)
        Y.append(ratios)
        
    plt.plot(X,Y)
    plt.xlabel("Dimension D")
    plt.ylabel("Ratios")
    plt.show()
        
def plot_method_b():
    X = []
    Y = []
    for i in [5,10,15,100,150,200,256]:
        top = time()
        
        [x,y] = read_database(i)
        ratios = compute_ratios_2(x,y)

        print("temps de calcul =", time()-top)

        X.append(i)
        Y.append(ratios)
        
    plt.plot(X,Y)
    plt.xlabel("Dimension D")
    plt.ylabel("Ratios")
    plt.show()

def read_database_2(dim):
    x = []
    y = []
    for image_name in os.listdir(folder):
        image = cv2.imread(folder + image_name,cv2.IMREAD_GRAYSCALE)
        hist = cv2.calcHist(image, [0, 1, 2], None, [dim, dim, dim], [0, 256, 0, 256, 0, 256])
        if image_name.find("_original_") != -1:
            y.append(normalise(transform_hist_vect_2(hist, dim)))
        else:
            x.append(normalise(transform_hist_vect_2(hist, dim)))
    return [x,y]
    
def transform_hist_vect_2(hist, dim):
    vect = []
    for i in range(dim):
        for j in range(dim):
            for k in range(dim):
                vect.append(hist[i][j][k])
    return vect            

def compute_ratios_2(x,y):
    z=hell_distance(x,y) # la distance euclidienne. 
        # 'sqeuclidean' donnerait la dist euclidienne au carre 
    minimums=z.min(1)
    maximums=z.max(1)
    ratios=minimums/maximums
    return ratios
        
def plot_method_b_2():
    X = []
    Y = []
    for i in [2,4,8,10,13,16,20]: #[8, 64, 200, 256, 512, 1000, 2197, 4096, 8000]
        top = time()
        
        [x,y] = read_database_2(i)
        ratios = compute_ratios_2(x,y)

        print("Dimension :", i**3)
        print("Temps de calcul :", time()-top)

        X.append(i**3)
        Y.append(ratios)
        
    plt.plot(X,Y)
    plt.xlabel("Dimension D")
    plt.ylabel("Ratios")
    plt.show()
    
#N = 3051
#M = 157