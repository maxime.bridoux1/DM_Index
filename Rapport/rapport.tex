\documentclass[11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{listings}
\usepackage{array}
\usepackage{fullpage}

\parskip=0.5\baselineskip

\lstset{
	basicstyle= \sffamily,
	columns=fullflexible,
	frame=lb,
	frameround=fftf,
	language=Python,
	numbers=left,}

%
\begin{document}
%
\title{Indexation: Rapport}
%
\author{Tom Bachard \and Maxime Bridoux}
%
\date{\today}
%
\maketitle
%

\section{Introduction}

	On cherche dans ce rapport à étudier le phénomène appelé \emph{Malédiction de la dimensionnalité}. On possède une collection de N vecteurs qui représentent les données d'une base et M vecteurs qui représentent les requêtes faites à cette base. Ces vecteurs sont tous de dimension D. On va montrer comment les données se concentrent en grande dimension dans un espace restreint à l'aide d'un calcul. 
	
	On va calculer pour chaque requête sa distance à chaque vecteur de la base avant d'en extraire la distance minimale et la distance maximale. L'étude du ratio de ces deux valeurs va pouvoir permettre de conclure: si celui-ci tend vers 1, c'est que les distances étudiées sont proches et donc les vecteurs aussi. 
	
	La première partie réalise cette étude sur des données synthétiques en utilisant des vecteurs générés aléatoirement. La seconde partie utilise des données réelles constituées d'un ensemble d'images dont certaines ont été retouchées a posteriori.
	
	Les codes fournis dans ce rapport sont écrit dans le langage \lstinline|Python|. Les exécutions ont été faites sur une machine Ubuntu 16.04 TLS, dotée d'un processeur i5-7300HQ CPU @ 2.50GHz * 4 et d'une carte graphique GeForce GTX 1050 Ti.






\section*{Partie A: données synthétiques}






\subsection*{Question 1}

	On fait varier $D$ de 16 à $2^{13}$. On a $M=100$ vecteurs requêtes et $N=1000$ vecteurs de la base générés aléatoirement. On obtient alors en figure \ref{1.1} $M=100$ courbes représentant les ratios de la distance minimale sur la distance maximale selon la dimension D des vecteurs.
	
	 \begin{figure}[h]
		\centering
	 	\includegraphics[scale=0.48]{1_1.png}
	 	\caption{Évolution des ratios lorsque D augmente pour $M=100$ et $N=1000$.}
	 	\label{1.1}
	 \end{figure}

	Les temps de calcul (en secondes) pour chaque graphique sont donnés en figure \ref{1_tab}.
	
	On décide de maintenant d'utiliser une valeur de N beaucoup plus grande: on augmente l'ordre de grandeur des calculs en multipliant N par 10, soit $N=10000$. Le graphique obtenu est représenté en figure \ref{1.2}.

	\begin{figure}[h]
		\centering
	 	\includegraphics[scale=0.48]{1_2.png}
	 	\caption{Évolution des ratios lorsque D augmente pour $M=100$ et $N=10000$.}
	 	\label{1.2}
	 \end{figure}
	
	\begin{figure}[!h]
		\centering
		\begin{tabular}{c|c|c}
			Dimension & Temps en secondes & Temps en secondes\\
			D & $M = 100, N = 1000$ & $M = 100, N = 10000$\\ \hline
			16 & 0.002 & 0.044\\
			32 & 0.003 & 0.032\\
			64 & 0.007 & 0.060\\
			128 & 0.012 & 0.114\\
			256 & 0.026 & 0.237\\
			512 & 0.051 & 0.487\\
			1024 & 0.104 & 0.996\\
			2048 & 0.204 & 2.006\\
			4096 & 0.411 & 3.970
		
		\end{tabular}
		\caption{Temps de calcul selon la dimension et le choix de M et N.}
		\label{1_tab}
	\end{figure}


	\begin{figure}[!h]
		\centering
		\includegraphics[scale=0.5]{1_3.png}
		\caption{Évolution des ratios lorsque M augmente pour $D=256$ et $N=1000$.}
		\label{1.3}
	\end{figure}
	 
	On remarque notamment que le temps augmente linéairement à la fois selon D et N.

	Enfin, on va faire évoluer M. Pour cela, on va fixer $N=1000$ et $D=256$: cette dernière valeur donne les plus grandes zones de fluctuation des ratios. Le graphique obtenu calculé en 3.22 secondes est exposé en figure \ref{1.3}. On y remarque notamment que les ratios diminuent avec le nombre de requêtes. C'est logique car plus il y a de requêtes, plus la probabilité que les distances minimales et maximales sont éloignées augmente.
	

	 
	 
	 
\subsection*{Question 2}

	Afin de générer un ensemble de vecteurs dans le cadran positif de l'hyper-sphère unité de dimension D on commence par générer un ensemble de vecteurs de dimension D. On va ensuite les normaliser pour qu'ils se situent sur l'hyper-sphère unité pour enfin mettre tous leurs coefficients en valeur absolue pour qu'ils soient dans le cadran positif.

\begin{lstlisting}
import numpy as np
import scipy.spatial.distance as dist
from math import sqrt

def generate_vectors(N,D):
    x = np.random.randn(N,D) #generation aleatoire
    zero = [np.zeros(D) for i in range(N)] #le vecteur nul
    norms = dist.cdist(x, zero, 'sqeuclidean') #norme 2 de tous les vecteurs
    for i in range(N):
        a = sqrt(norms[i][0]) #calcul de la racine carree de la norme pour normaliser
        for j in range(D):
            x[i][j] = abs(x[i][j]/a) #normalisation et mise en valeur absolue
    return x;
\end{lstlisting}






\subsection*{Question 3}

	On reprend alors les mêmes expériences que pour la 1ère question, en considérant des vecteurs de l'hyper-sphère unité. Afin de comparer avec la situation précédente, on superpose les deux graphiques en figure \ref{3.1}. La figure \ref{3.2} effectue la même comparaison pour $M=100$ et $N=10000$. Enfin la figure \ref{3.3} effectue la comparaison pour M variant. 
	
	\begin{figure}[!h]
		\centering
	 	\includegraphics[scale=0.36]{3_1.png}
	 	\caption{Comparaison des ratios lorsque D augmente pour $M=100$ et $N=1000$.}
	 	\label{3.1}
	 \end{figure}
	

	 \begin{figure}[!h]
		\centering
	 	\includegraphics[scale=0.36]{3_2.png}
	 	\caption{Comparaison des ratios lorsque D augmente pour $M=100$ et $N=10000$.}
	 	\label{3.2}
	 \end{figure}

	\begin{figure}[!h]
		\centering
	 	\includegraphics[scale=0.36]{3_3.png}
	 	\caption{Évolution des ratios lorsque M augmente pour $D=256$ et $N=1000$.}
	 	\label{3.3}
	 \end{figure}
	 
	 On remarque que lorsque l'on considère les vecteurs de l'hyper-sphère unité, les ratios obtenus tendent vers 1 plus rapidement en moyenne quelle que soit la situation. La figure \ref{3_tab} donne le temps de calcul pour chaque dimension.
	
	 
\begin{figure}[!h]
	\centering
	\begin{tabular}{c|c|c}
		Dimension & Temps en secondes & Temps en secondes\\
		D & $M = 100, N = 1000$ & $M = 100, N = 10000$\\ \hline
		16 & 0.023 & 1.066\\
		32 & 0.037 & 1.689\\
		64 & 0.077 & 3.726\\
		128 & 0.150 & 8.440\\
		256 & 0.314 & 17.534\\
		512 & 0.669 & 37.710\\
		1024 & 1.393 & 76.459\\
		2048 & 2.806 & 148.962\\
		4096 & 5.651 & 319.012
		
	\end{tabular}
	\caption{Temps de calcul selon la dimension et le choix de M et N.}
	\label{3_tab}
\end{figure}







\subsection*{Question 4}

	 On cherche maintenant à créer des hubs et anti-hubs sur le cadran positif de l'hyper-sphère unité. Les vecteurs considérés sont tels que $X=(x_{i})_{i<D}$ où $\forall i, x_{i} \in [0,1]$. On pressent donc que si $X$ a des coordonnées proches de $0.5$, celui ci sera plus proche de la majorité des vecteurs que s'il avait des coordonnées proches de $0$ ou $1$. On va utiliser cette propriété pour construire nos hubs et anti-hubs.
	 
	 On aimerait donc proposer comme hub le vecteur $X_{0.5} = [0.5, ..., 0.5]$. Néanmoins celui-ci n'est pas sur la sphère unité: on propose donc son normalisé, $X_{hub} = [\frac{1}{\surd D}, ..., \frac{1}{\surd D}]$. On peut également considérer en grande dimension des variantes: $X_{hub_{1}} = [0, \frac{1}{\surd D-1}, ..., \frac{1}{\surd D-1}]$, $X_{hub_{2}} = [0, 0,\frac{1}{\surd D-2}, ..., \frac{1}{\surd D-2}]$, etc.
	 
	 En parallèle, on aimerait proposer comme anti-hub le vecteur $X_{0 ou 1}$ où $\forall i, x_{i} \in [0,1]$. On retient donc le vecteur $X_{anti-hub} = [1,0, ..., 0]$ pour que celui-ci ait une norme de 1. On peut également retenir les vecteurs $X_{anti-hub_{i}} = [0, ...,0,1,0,...,0]$ avec $x_{i} = 1$ et $\forall j \neq i, x_{j} = 0$. 
	 
	 
	 
	 
	 
	 
	 
\subsection*{Question 5}

	La fonction suivante donne le rang moyen dans le classement d'un vecteur de la base des distances ainsi que l'écart type associé lors des M requêtes:
	
\begin{lstlisting}
from statistics import mean, stdev

def compute_ranks(M, z, vect_pos):
    rangs = []
    for i in range(M):
        lrangs_dist = np.argsort(z[:,i])
        #lrangs_dist[k] donne l'indice de l'element classe en k-eme position
        #trouver le classement d'un element d'indice i revient a resoudre lrangs_dist[k] = i
        #i.e. i = np.where(rangs_dist==k)[0][0]
        rang_dist = np.where(lrangs_dist==vect_pos)[0][0]
        rangs.append(rang_dist)
    return [mean(rangs), stdev(rangs)]
\end{lstlisting}  

	L'argument \emph{vect\_pos} donne la position du vecteur dans x dont on veut calculer le classement moyen. Puisque le hub et l'anti-hub sont ajoutés à la fin de x de taille N, ceux-ci sont les N+1-èmes et N+2-èmes éléments du vecteur. Leurs classements moyens et les écarts-types associés sont donc calculés avec \lstinline|compute_ranks(M, z, N)| et \lstinline|compute_ranks(M, z, N+1)|.
	
	Le calcul de ces deux fonctions donnent pour N=1000, M=100 et D=8192 le rang 1 pour le hub, avec un écart type-nul. L'anti-hub lui obtient le rang 1002 avec également un écart-type nul. On en conclut donc que quelque soit la requête M, ces deux vecteurs que l'on a crées se placent respectivement premier et dernier: ce sont bien un hub et un antihub.








\subsection*{Question 6}

	L'objectif de cette partie est de créer un vecteur qui va partir du hub et progressivement être modifié pour finir par être confondu par le anti-hub, afin de savoir à partir de quel moment celui-ci devient un anti-hub. Le code suivant trace le rang moyen de ce vecteur selon ses pas:

\begin{lstlisting}
x = generate_vectors(N,D) # les donnees
y = generate_vectors(M,D) # les requetes
nb_step = 10 #nombre d'etapes a parcourir

for step in range(nb_step):

    vector  = [ ((nb_step-1) - step) / (nb_step-1) * np.array(hub) 
            +  (step / (nb_step-1)) * np.array(antihub)]
            
    norm = dist.cdist(vector, zero, 'sqeuclidean')[0][0]
    vector[0] = vector[0] / norm #normalisation du vecteur
    
    x = np.append(x, vector) #ajout du vecteur
    x = x.reshape((N+1,D))
    
    
    z = dist.cdist(x,y,'euclidean') # la distance euclidienne. 
    
    results = np.append(results, compute_ranks(M, z, N))
    x = np.delete(x, -1, 0) # supprime le dernier vecteur
    
results = results.reshape((nb_step,2))
plt.plot(results[:,0])
\end{lstlisting}

Les deux graphiques en figure \ref{6.1} et \ref{6.2} présentent le rang de ce vecteur selon si les vecteurs sont restreints au cadrant positif (en vert) ou non (en bleu). Le calcul a été effectué pour une distance entre hub et anti-hub de 10 étapes puis 200 pour affiner (pour une durée d'environ 70s par courbe).

	On remarque tout de suite que nos hubs et anti-hubs sont efficaces sur le cadran positif, et que la séparation est très nette, même en 200 étapes. En revanche ces deux vecteurs sont quelconques quand on considère toute l'hyper-sphère unité. Le vecteur ajouté passe quand même par une phase où celui-ci est un anti-hub.

\begin{figure}[h]
		\centering
	 	\includegraphics[scale=0.4]{6_1.png}
	 	\caption{Rang moyen du vecteur ajouté lorsque celui-ci se déplace en 10 étapes.}
	 	\label{6.1}
	 \end{figure}
	 
	 \begin{figure}[h]
		\centering
	 	\includegraphics[scale=0.4]{6_2.png}
	 	\caption{Rang moyen du vecteur ajouté lorsque celui-ci se déplace en 200 étapes.}
	 	\label{6.2}
	 \end{figure}
	 
	 
	 

\section*{Partie B: données réelles}

	Les données considérées sont désormais des données réelles constituées d'images réelles ou retouchées. La lecture se fait par la fonction \lstinline|read_database| ci-dessous, où la variable globale \emph{folder} contient l'adresse du dossier \emph{Copydays}. La fonction génère les x et y précédents avec la dimension $D = \emph{dim}$. La fonction auxiliaire \lstinline|transform_hist_vect| formate les histogrammes extraits des images en simples listes de coordonnées. Les vecteurs requêtes sont ceux contenant le mot clé \lstinline|"_original_"| dans leur nom. On obtient alors pour toute cette partie une base de données de taille $N=3212$ et $M=157$ vecteurs requêtes.



\subsection*{Question 7}

	Pour les histogrammes en niveau de gris, on a la fonction \lstinline|read_database| suivante:

\begin{lstlisting}
def read_database(dim):
    x = []
    y = []
    for image_name in os.listdir(folder):
        image = cv2.imread(folder + image_name,cv2.IMREAD_GRAYSCALE)
        hist = cv2.calcHist([image],[0],None,[dim],[0,256])
        if image_name.find("_original_") != -1:
            y.append(transform_hist_vect(hist))
        else:
            x.append(transform_hist_vect(hist))
    return [x,y]
\end{lstlisting}

\newpage

Ceci nous permet de créer les graphiques suivants: pour la méthode a), la figure \ref{7.1} présente l'évolution des 157 ratios lorsque D augmente; la figure \ref{7.2} donne le ratio moyen pour plus de lisibilité. Les figures \ref{7.3} et \ref{7.4} présentent les même résultats pour la méthode b).

\begin{figure}[!h]
		\centering
	 	\includegraphics[scale=0.38]{7_1.png}
	 	\caption{Ratios des distances pour les 157 vecteurs requêtes pour la méthode a).}
	 	\label{7.1}
	 \end{figure}
	 
	 \begin{figure}[!h]
		\centering
	 	\includegraphics[scale=0.38]{7_2.png}
	 	\caption{Ratios moyen des distances pour les 157 vecteurs requêtes pour la méthode a).}
	 	\label{7.2}
	 \end{figure}
	 
	 \begin{figure}[!h]
		\centering
	 	\includegraphics[scale=0.38]{7_3.png}
	 	\caption{Ratios des distances pour les 157 vecteurs requêtes pour la méthode b).}
	 	\label{7.3}
	 \end{figure}
	 
	 \begin{figure}[!h]
		\centering
	 	\includegraphics[scale=0.37]{7_4.png}
	 	\caption{Ratios moyen des distances pour les 157 vecteurs requêtes pour la méthode b).}
	 	\label{7.4}
	 \end{figure}




\subsection*{Question 8}

	En appliquant la même méthode b), on obtient en figure \ref{8.1} et \ref{8.2} les ratios des distances pour les histogrammes en couleurs. Ci-dessus le code utilisé:
	
	\begin{figure}[h]
		\centering
	 	\includegraphics[scale=0.37]{8_1.png}
	 	\caption{Ratios des distances pour les 157 vecteurs requêtes pour la méthode b).}
	 	\label{8.1}
	 \end{figure}
	 
	 \begin{figure}[!h]
		\centering
	 	\includegraphics[scale=0.37]{8_2.png}
	 	\caption{Ratios moyen des distances pour les 157 vecteurs requêtes pour la méthode b).}
	 	\label{8.2}
	 \end{figure}
	
\begin{lstlisting}
def read_database(dim):
    x = []
    y = []
    for image_name in os.listdir(folder):
        image = cv2.imread(folder + image_name,cv2.IMREAD_GRAYSCALE)
        hist = cv2.calcHist(image, [0, 1, 2], None, [dim, dim, dim], [0, 256, 0, 256, 0, 256])
        cv2.normalize(hist,hist, norm_type=cv2.NORM_L2)
        if image_name.find("_original_") != -1:
            y.append(transform_hist_vect(hist, dim))
        else:
            x.append(transform_hist_vect(hist, dim))
    return [x,y]
    
def transform_hist_vect(hist, dim):
    vect = []
    for i in range(dim):
        for j in range(dim):
            for k in range(dim):
                vect.append(hist[i][j][k])
    return vect            

def compute_ratios(x,y):
    z=distance.cdist(x,y,'sqeuclidean')
    minimums=z.min(1)
    maximums=z.max(1)
    ratios=minimums/maximums
    return ratios
        
def plot_method_b(moyenne = False):
    X = []
    Y = []
    for i in [2,4,8,10,13,16,20]: #[8, 64, 200, 256, 512, 1000, 2197, 4096, 8000]
        top = time()
        
        [x,y] = read_database(i)
        ratios = compute_ratios(x,y)

        print("Dimension :", i**3)
        print("Temps de calcul :", time()-top)

        X.append(i**3)
        if moyenne:
            Y.append(mean(ratios))
        else:
            Y.append(ratios)
        
    p1 = plt.plot(X,Y)
    plt.xlabel("Dimension D")
    plt.ylabel("Ratios")
    if moyenne:
        legend = "Ratios moyens des distances des 157 requetes methode b"
    else:
        legend = "Ratios des distances des 157 requetes methode b"
    plt.legend([p1[0]],[legend], loc = "best")
    plt.show()
\end{lstlisting}

	 


\subsection*{Question 9}

	On superpose maintenant les graphiques précédents aux résultats obtenus pour les données synthétiques. On garde toujours $N=3212$ et $M=157$ afin de pouvoir tirer le maximum d'informations de la base de données réelle. Les figures \ref{9.1} et \ref{9.2} montrent cette comparaison, où les traits verts montrent les résultats des données réelles et les traits rouges les données synthétiques.

	\begin{figure}[!h]
		\centering
	 	\includegraphics[scale=0.4]{9_1.png}
	 	\caption{Comparaison des ratios des distances pour les données réelles par la méthode b) et les données synthétiques.}
	 	\label{9.1}
	 \end{figure}
	 
	 \begin{figure}[!h]
		\centering
	 	\includegraphics[scale=0.4]{9_2.png}
	 	\caption{Comparaison des ratios moyens des distances pour les données réelles par la méthode b) et les données synthétiques.}
	 	\label{9.2}
	 \end{figure}
	 
	Une remarque immédiate est que le ratio des données synthétique est toujours bien plus proche de 1 que les données réelles.
	
\subsection*{Question 10}

	On redonne la formule de la distance d'Hellinger: 
	
\begin{center}
	$H(p, q) = \frac{1}{\sqrt{2}} \sqrt{\sum_i^n (\sqrt{p_i} - \sqrt{q_i})^2}$. 
\end{center}

	On l'implémente en python au travers de la fonction \lstinline|hellinger_squared| qui calcule cette distance au carré entre les vecteurs p et q. La normalisation est assurée par la fonction \lstinline|normalisation| et est faite dès la lecture des vecteurs de la base de données. 
	\bigskip
	
\begin{lstlisting}
def hellinger_squared(p, q):
    return np.sum((np.sqrt(p) - np.sqrt(q)) ** 2) / 2
    
def normalise(a):
    norm = sum(a)/2
    b = []
    for x in a:
        b.append(x/norm)
    return b
    
def hell_distance(x,y): #calcule la matrice des distances entre les vecteurs de x et y
    z = np.array([])
    for p in x:
        a = np.array([])
        for q in y:
            a = np.append(a, hellinger_squared(p,q))
        z = np.append(z, a)
    return np.reshape(z, ((len(x),len(y))))
\end{lstlisting}
	
	Les graphiques obtenus aux questions précédentes sont refaits en utilisant cette distance en figure \ref{10.1} et \ref{10.2} pour la question 7 méthode a), \ref{10.3} et \ref{10.4} pour la méthode b) et enfin en figure \ref{10.5} et \ref{10.6} pour la question 8.
	Les temps de calcul peuvent être un problème: pour les 4 premiers graphiques produits, on comptait environ 20s de calcul par dimension. Ce nombre montait jusqu'à 240s pour la dimension 8000 lors des deux derniers graphiques de la question 8 (ce temps de calcul augmente toujours linéairement avec la dimension).


	\begin{figure}[!h]
		\centering
	 	\includegraphics[scale=0.6]{10_1.png}
	 	\caption{Ratios des distances par Hellinger, histogrammes gris, méthode a).}
	 	\label{10.1}
	 \end{figure}
	 
	 \begin{figure}[!h]
		\centering
	 	\includegraphics[scale=0.6]{10_2.png}
	 	\caption{Ratios moyens des distances par Hellinger, histogrammes gris, méthode a).}
	 	\label{10.2}
	 \end{figure}
	 
	 \begin{figure}[!h]
		\centering
	 	\includegraphics[scale=0.6]{10_3.png}
	 	\caption{Ratios des distances par Hellinger, histogrammes gris, méthode b).}
	 	\label{10.3}
	 \end{figure}
	 
	 \begin{figure}[!h]
		\centering
	 	\includegraphics[scale=0.6]{10_4.png}
	 	\caption{Ratios moyens des distances par Hellinger, histogrammes gris, méthode b).}
	 	\label{10.4}
	 \end{figure}
	 
	 \begin{figure}[!h]
		\centering
	 	\includegraphics[scale=0.6]{10_5.png}
	 	\caption{Ratios des distances par Hellinger, histogrammes couleur, méthode b).}
	 	\label{10.5}
	 \end{figure}
	 
	 \begin{figure}[!h]
		\centering
	 	\includegraphics[scale=0.6]{10_6.png}
	 	\caption{Ratios moyens des distances par Hellinger, histogrammes couleur, méthode b).}
	 	\label{10.6}
	 \end{figure}

\newpage
\
\newpage
\
\newpage

\subsection*{Question 11}
	
	Les résultats des questions précédentes montrent que les données réelles fournies ont une légère tendance à suivre les résultats donnés par les données synthétiques. Néanmoins cette tendance est à nuancer pour plusieurs raisons. 
	
	Tout d'abord, les ratios calculés sont nettement plus faibles, comme le montre la question 9. 
	
	Ensuite, le peu de données fournies, face à la génération aisée de données synthétiques, ne permet pas d'avoir autant confiance en les ratios calculés pour les données réelles. 
	
	Enfin, et c'est la principale différence, on voit que les données réelles et synthétiques ne suivent pas la même loi de distribution. En effet, la comparaison des ratios montre que les distances minimales et maximales des données réelles ne se rapprochent pas aussi vite que pour les données synthétiques. Ces données réelles sont donc loin d'une répartition uniforme ou gaussienne dans l'espace.
	
	On peut l'interpréter par le fait que malgré la dimension élevée, certaines images restent très proches les unes des autres dans des noyaux tels que l'augmentation de la dimension peine à les séparer. Cela montre également qu'en augmentant le nombre d'image dans la base de données, on n'augmenterait pas nécessairement les ratios pour nos différentes requêtes.
	
	Une autre remarque peut être faite sur la façon dont les ratios de distances des données réelles (quelle que soit la distance considérée): ceux-ci sont beaucoup plus éparpillés autour de la moyenne que pour les données synthétiques. Un exemple flagrant est en figure \ref{9.1}, où on voit très bien à quel point les ratios des données synthétiques restent très proches de leur moyenne tandis que les ratios des données réelles sont beaucoup aléatoires. 
	
	On peut tenter d'expliquer ce phénomène par encore un argument spatial: la façon dont ces noyaux s'organisent dans l'espace reste très aléatoire. Les données auxquelles nous sommes confrontés restent en fin de compte très aléatoires. En effet, les images retouchées resteront très proches de l'image originale, mais on ne peut même pas prédire d'ordre de grandeur de distance entre ces images et une autre complètement quelconque.
	
	Pour conclure, il est donc nécessaire de préciser que la façon dont on traite des données réelles est très différente de la façon de traiter des données synthétiques, et les expériences faites au fur et à mesure de ce devoir en sont un bon exemple. Ces dernières montrent aussi les difficultés à travailler sur des données réelles à cause des ratios bas malgré les dimensions mises en jeu. Les temps de calcul, qui augmentent aussi rapidement avec la dimension peuvent être un frein: il faut un bon compromis entre ratio moyen et temps de calcul.
	
	
	
	
	

\end{document}