import numpy as np
import scipy.spatial.distance as dist
import matplotlib.pyplot as plt
from time import time
from math import sqrt

def generate_vectors(N,D):
    x = np.random.randn(N,D)
    zero = [np.zeros(D) for i in range(N)]
    norms = dist.cdist(x, zero, 'sqeuclidean')
    for i in range(N):
        a = sqrt(norms[i][0])
        for j in range(D):
            x[i][j] = x[i][j]/a
    return x;

#M=100
#N=10000
#
#
#X = []
#Y = []
#
#for i in range(4,13):
#    top = time()
#    D = 2**i
#    print("D =", D)
#    x=np.random.randn(N,D) # les donnees
#    y=np.random.randn(M,D) # les requetes
#    z=dist.cdist(x,y,'euclidean') # la distance euclidienne. 
#    # 'sqeuclidean' donnerait la dist euclidienne au carre 
#    
#    minimums=z.min(1)
#    maximums=z.max(1)
#    ratios=minimums/maximums
#    
#    print("temps de calcul =", time()-top)
#    X.append(D)
#    Y.append(ratios)
#    
#p1= plt.plot(X,Y,"r-", label = "Vecteurs aléatoires")
#plt.xlabel("Dimension D")
#plt.ylabel("Ratios")
#plt.show()
#
#X = []
#Y = []
#
#for i in range(4,13):
#    top = time()
#    D = 2**i
#    print("D =", D)
#    x=generate_vectors(N,D) # les donnees
#    y=generate_vectors(M,D) # les requetes
#    z=dist.cdist(x,y,'euclidean') # la distance euclidienne. 
#    # 'sqeuclidean' donnerait la dist euclidienne au carre 
#    
#    minimums=z.min(1)
#    maximums=z.max(1)
#    ratios=minimums/maximums
#    
#    print("temps de calcul =", time()-top)
#    X.append(D)
#    Y.append(ratios)
#    
#p2 = plt.plot(X,Y, "g-", label = "Vecteurs de l'hyper-sphère unité")
#plt.xlabel("Dimension D")
#plt.ylabel("Ratios")
#plt.legend([p1[0],p2[0]],["Vecteurs aléatoires","Vecteurs de l'hyper-sphère unité"], loc = 'best')
#plt.show()

X = []
Y = []
D = 512
N = 1000
top = time()
for M in range(1,100):
    x=np.random.randn(N,D) # les donnees
    y=np.random.randn(M,D) # les requetes
    z=dist.cdist(x,y,'euclidean') # la distance euclidienne. 
    # 'sqeuclidean' donnerait la dist euclidienne au carre 
    
    minimums=z.min(1)
    maximums=z.max(1)
    ratios=minimums/maximums
    
    
    X.append(M)
    Y.append(ratios)
print("temps de calcul =", time()-top)
p1= plt.plot(X,Y,"r-", label = "Vecteurs aléatoires")
plt.xlabel("Nombre de requêtes M")
plt.ylabel("Ratios")
plt.show()

X = []
Y = []
top = time()
for M in range(1,100):
    x=generate_vectors(N,D) # les donnees
    y=generate_vectors(M,D) # les requetes
    z=dist.cdist(x,y,'euclidean') # la distance euclidienne. 
    # 'sqeuclidean' donnerait la dist euclidienne au carre 
    
    minimums=z.min(1)
    maximums=z.max(1)
    ratios=minimums/maximums
    
    
    X.append(M)
    Y.append(ratios)
print("temps de calcul =", time()-top)
p2 = plt.plot(X,Y, "g-", label = "Vecteurs de l'hyper-sphère unité")
plt.xlabel("Nombre de requêtes M")
plt.ylabel("Ratios")
plt.legend([p1[0],p2[0]],["Vecteurs aléatoires","Vecteurs de l'hyper-sphère unité"], loc = 'best')
plt.show()